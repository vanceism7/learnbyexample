# Servant + Selda

The following is an example of using Servant to serve a web api  
in combination with the Selda package to provide database capabilities.

```haskell
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}

module ServantSelda.Example where

import Data.Text
import Data.Time (UTCTime)
import Servant.API
import Servant
import Database.Selda.SQLite
import qualified Database.Selda as Db
import qualified ServantSelda.User as U

-- | Route Definition
type UserApi
     = "users" :> Get '[JSON] [U.User]

-- | Boilerplate
userApi :: Proxy UserApi
userApi = Proxy

-- | Response function
server :: Server UserApi
server =
  getUsers
  where
    getUsers :: Handler [U.User]
    getUsers = withSQLite "path/to/your.db" $ do
      users <- Db.query $ Db.select U.usersTable
      return users
```

As you can see above, `withSQLite` is our bread winner here. Initially, I thought  
I would need to use `SeldaT` around Servant's Handler Monad in order to gain the database  
functionality, but this turned out to not be the case.  
`withSQLite` takes a `SeldaT` computation, and unwraps the result into your final  
`Handler` monad, so there's nothing to it but to query your data and return it.  

Hope this helps!
