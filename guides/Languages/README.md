# Language Guides

This is a set of guides for learning different programming languages. 
When learning a new language, I think the experience for a lot of people may be similar to 
sloshing around/skimming through tutorials and docs in an effort to learn 
the "core essentials" of the language.

My aim with these guides is to create a set of exercises that capture that core essence
of what one needs to know how to do in order to be productive in that language.
Some programming languages can differ in many ways, but our end goal in writing a program
is always the same - to create software that automates tasks while reducing human error.

## Core language concepts

The following list is a set of concepts one needs to know to be productive in a language.
Included alongside the concepts are exercises which, when successfully completed, demonstrate
competency in that concept area.

##### 0. Project setup
   - Ex 1. How to build a basic project in the language
---

##### 1. Get some output in the console
   - Ex 1.  Write a basic hello world  
---

##### 2. Declare primitive types and function
   - Ex 1. Write calculator functions (+, -, *, /)
---

##### 3. Create Data structures/objects**
   - Ex 1. Create a person record/object.
---

##### 4. Control Structures
   - Ex 1. If, switch/case/pattern matching, loops/recursion
---

##### 5. Import Libs
   - Ex 1. Create a data structure in a module and import it in the main module
---

##### 6. Work with arrays/lists, dictionaries, tuples
   - Ex 1. List - Show summation of int list
   - Ex 2. List - Filter a list of strings
   - Ex 3. Dictionary - Group a selection of people by gender
   - Ex 4. Tuples - Show a function which returns two values
---

##### 7. The package manager
   - Ex 1. Add external dependencies to your program
---

##### 8. Serialization
   - Ex 1. Encode/Decode Json
