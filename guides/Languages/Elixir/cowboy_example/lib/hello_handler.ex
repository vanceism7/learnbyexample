defmodule HelloHandler do
  @moduledoc """
  Documentation for `HelloHandler`.
  """

  def init(req0, state) do
    req = :cowboy_req.reply(200,
      %{"content-type" => "text/plain"},
      "Hello new Elixir site!",
      req0)
    { :ok, req, state }
  end
end
