defmodule CowboyTest2 do
  @moduledoc """
  Documentation for `CowboyTest2`.
  """
  def init(init_arg) do
    {:ok, init_arg}
  end

  def child_spec(opts) do
    %{
      id: CowboyTest2,
      start: {__MODULE__, :start_link, [opts]},
      shutdown: 5_000,
      restart: :permanent,
      type: :worker
    }
  end

  def start_link(_arg) do
    dispatch =
      :cowboy_router.compile([
        {:_,
         [
           {"/", HelloHandler, []}
         ]}
      ])

    IO.puts("Starting server listening on localhost:8080")

    {:ok, _} =
      :cowboy.start_clear(
        :my_http_listener,
        [{:port, 8080}],
        %{env: %{dispatch: dispatch}}
      )
  end
end
