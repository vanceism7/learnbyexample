# Cowboy Example

This directory contains a minimal example of a cowboy server written in elixir. We have a
`shell.nix` file in here so you can automatically have the elixir tooling setup by using
`nix-shell` if needed. 

Aside from that, you run the server by simply running `make` to build a release of the app and spin
it up. It starts up a server on http://localhost:8080. This combined with the cowboy tutorial 
should be enough to get your feet wet with a cowboy server.
