with import <nixpkgs> {};

stdenv.mkDerivation rec {
  name = "elixir-env";

  buildInputs = [ 
    elixir
  ];

  shellHook = ''
    echo "Elixir Shell Ready"
  '';

}
