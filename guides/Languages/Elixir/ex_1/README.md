# Ex_1

This first exercise shows how to setup a basic Hello World elixir application

The basic setup goes as follows:

## Step 1. - Create our elixir project
`mix new ex_1 --module Main`

This creates a new mix project called "ex_1". We use `--module Main` to name our main module `Main`.

## Step 2. - The project entry point
`Mix` compiles and runs our application as well, but it will take some setup before `mix run` actually works

In `mix.exs`, add the following line to the `application` function: `mod: {Main, []},`.
The final result should look like this:

```
def application do
  [
    mod: {Main, []},
    extra_applications: [:logger]
  ]
```

The above sets our projects entry module to the `Main` module. Otherwise, nothing runs

## Step 3. - The `start` function
`Mix` looks for a function called `start` in the entry module to start the app. 
Go to your `lib/main.ex` module and add the following function:

```
def start(_type, _args) do
  IO.puts("Hello World!")
  Task.start(fn -> :timer.sleep(10) end)
end
```

The `Task.start` line is only used so the app doesn't return an error code

Thats it! You've created your first elixir application