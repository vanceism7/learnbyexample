defmodule Main do
  @moduledoc """
  Documentation for `Main`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Main.hello()
      :world

  """
  def hello do
    :world
  end

  @doc """
  Main App Entry point
  """
  def start(_type, _args) do
    IO.puts("Hello World!")
    Task.start(fn -> :timer.sleep(10) end)
  end
end
