with import <nixpkgs> {};

stdenv.mkDerivation rec {
  name = "haskell-env";

  buildInputs = [ 
    elixir
    glibcLocales
  ];
}
