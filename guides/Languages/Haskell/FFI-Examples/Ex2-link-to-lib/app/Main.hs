module Main where

import Foreign.C

foreign import ccall "foo.h add" add :: CInt -> CInt -> IO CInt

foreign import ccall "foo.h sub" sub :: CInt -> CInt -> IO CInt

main :: IO ()
main = do
  result1 <- add 10 15
  result2 <- sub 12 4
  putStrLn "Hello, Haskell!"
  putStrLn $ "Add 10 and 15 and you get " <> show result1
  putStrLn $ "12 - 4 = " <> show result2
