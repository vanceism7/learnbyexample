#include "foo.h"

// Adds two numbers, with a twist ;)
int add(int a, int b)
{
  return a * a + b;
}

// Subtracts numbers in a boring way
int sub(int a, int b)
{
  return (3 * a) - (2 * b);
}