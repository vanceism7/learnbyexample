#ifndef FOO_H
#define FOO_H

// Adds two numbers, with a twist ;)
int add(int a, int b);

// Subtracts numbers in a boring way
int sub(int a, int b);

#endif