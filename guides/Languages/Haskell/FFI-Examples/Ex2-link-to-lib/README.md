# Linking to a lib file

In this example, we first build a lib file by compiling `foo.c` located in the `libfoo` directory,
then we build our haskell code using cabal. In order for this to build successfully, we need the 
following options set either in the cabal file, or the cabal.project file (`cabal.project.local` 
here):
```
extra-lib-dirs: ./libfoo
extra-include-dirs: ./libfoo
```

In our main cabal file, we need to reference our lib file (`foo` in this example) like this:
```
  extra-libraries: foo
```

Look at `haskell-ffi-test.cabal` and `cabal.project.local` to see how these settings have been
added. Foo must be compiled with the c compiler prior to running `cabal build`, or linking will
fail. The `Makefile` is setup to do this automatically  