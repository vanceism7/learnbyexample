# Linking directly to a c file

In this example, we interface with C directly through the `foo.c` file.  

In order for this to build successfully, we need to add the following line to our cabal file
```
  c-sources: ./app/foo.c
```

Look at `haskell-ffi-test.cabal` to see how these settings have been added. 