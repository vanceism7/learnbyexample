module Main where

import Foreign.C

foreign import ccall "foo.h add" add :: CInt -> CInt -> IO CInt

foreign import ccall "foo.h sub" sub :: CInt -> CInt -> IO CInt

main :: IO ()
main = do
  result1 <- add 15 35
  result2 <- sub 43 9
  putStrLn "Hello, Haskell!"
  putStrLn $ "Add 15 and 35 and you get " <> show result1
  putStrLn $ "43 - 9 = " <> show result2
