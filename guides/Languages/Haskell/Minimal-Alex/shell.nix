with import <nixpkgs> {};

stdenv.mkDerivation rec {
  name = "haskell-env";

  buildInputs = [ 
    haskellPackages.alex
    haskellPackages.happy
    gmp
  ];

  shellHook = ''
    export LD_LIBRARY_PATH+=:${zlib}/lib:${gmp}/lib
    echo "Haskel Shell Ready"
  '';
}
