module Main where

import Lexer ( runLexer )

main :: IO ()
main = do
  putStrLn "Basic Math Lexer. Enter a statement (such as 3 + 4): "
  s <- getLine
  
  putStrLn $ "You entered " <> s
  print (runLexer s)
