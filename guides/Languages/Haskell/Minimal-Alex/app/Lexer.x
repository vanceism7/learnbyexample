{
module Lexer (runLexer) where
}

%wrapper "basic"

$digit = 0-9            -- digits
$alpha = [a-zA-Z]       -- alphabetic characters

tokens :-

  $white+                        ;
  $digit+                        { \s -> Int (read s) }
  [\+\-\*\/]               { \s -> Sym (head s) }

{
-- Each action has type :: String -> Token

-- The token type:
data Token
  = Sym Char
  | Int Int
  deriving (Eq, Show)

-- The lexing function (atleast for the "basic" parser)
runLexer = alexScanTokens
}
