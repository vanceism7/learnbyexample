## Cross Building Docker images

So I've been trying to get my own cloud server up and running these days. I wanted to do this using kubernetes because
its a container management system and I'm a big fan of using containers to keep dependencies from contaminating each other.  

My server is a raspberry pi 4, so that means it's uses an ARM processor instead of the standard x86.  
This means I have to make sure to build the docker images using ARM compiled apps, this doesn't work however because
x86 can't build docker images which use ARM apps. Anyways!  

So how do we do it?  

### TLDR.
Download these packages
- `qemu-user-static`
- `binfmt-qemu-static`

`docker buildx create --new newBuilder` (You can use whatever name you want here, we're using newBuilder just cuz)
`docker buildx use newBuilder`
`docker buildx inspect --bootstrap`

`docker buildx build --platform linux/arm64,linux/arm/v7 -t user/yourImageName .`

### Step 1. Get some multi-arch dependencies
On Linux, there's two packages we need:
- `binfmt` (Something along the lines of this name)
- `qemu-user-static`

On debian based systems, I think these are easy enough to find using `apt-get`.
On Arch (my system), you can find them in the AUR under the following:
- `qemu-user-static`
- `binfmt-qemu-static`

### Step 2. Setup a new docker builder that can make multi-arch images
Once we download those, on docker version >= 19.04 (Something like that), do the following
`docker buildx create --new newBuilder` (You can use whatever name you want here, we're using newBuilder just cuz)
`docker buildx use newBuilder`
`docker buildx inspect --bootstrap`

### Step 3. Build some multi-arch images!
This should set you up a new builder capable of cross building docker images for different cpu architectures.

Now that you got your builder setup, instead of doing your standard `docker build ...`, we'll build like this:
`docker buildx build --platform linux/arm64,linux/arm/v7 -t user/yourImageName .`

For the list of platforms you're capable of building, you can run the command
`docker buildx ls`

So thats it! Hope this helps some poor desperate soul out there (took me long enough to figure out)
