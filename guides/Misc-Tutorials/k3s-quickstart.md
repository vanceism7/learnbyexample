# Getting started quick with k3s

## K3s Installation/Setup
- Normal Install (requires sudo when calling `k3s kubectl`)
```
curl -sfL https://get.k3s.io | sh -

# Allow non-sudo users after the fact
# Note: You will need to do this any time you restart the k3s service (i.e: sudo systemctl restart k3s)
sudo chmod 644 /etc/rancher/k3s/k3s.yaml
```

OR
- No-Sudo Install (allows you to use `k3s kubectl` without the `sudo` prefix):
```
curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC='--write-kubeconfig-mode=644' sh -
```

- Alias kubectl (if you want to ditch the `k3s` prefix)
Add the following line to your `~/.bash_aliases` file
```
alias kubectl="k3s kubectl"
```

### Note:
You will need to make sure the correct ports are open for your firewall config otherwise some pods may not work.  
The ports for k3
6443 - TCP  	- K3s agent nodes (Kubernetes API server)
8472 - UDP  	- K3s server and agent nodes (Flannel VXLAN Only) 
10250 - TCP 	- K3s server and agent nodes (Kubelet Metrics)
2379-2380 - TCP - K3s server nodes (Required only for HA with embedded etcd)
(See https://rancher.com/docs/k3s/latest/en/installation/installation-requirements/ for more info)

## Getting Helm to work
Installing helm from via get-helm script doesn't work out the gate.  
Run the following commands to fix it  
```
kubectl config view --raw >~/.kube/config

# If helm complains about permissions, run this command too
sudo chmod 600 config 
```

## Setup SSL for your cluster
1. Follow the guide here for installing cert-manager: https://cert-manager.io/docs/installation/kubernetes/

2. Get a certificate from lets-encrypt
To get an ssl certificate issued to us, we need to create an issuer and a certificate.  
  
The example below is a minimal setup to create both at once.  
Note: ClusterIssuers are easier to work with IMO because they apply to the entire cluster. This is a quick-start guide so deal with it lol.  
```
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt-issuer   # You can name this whatever you want
spec:
  acme:
    email: youremail@email.com
    # Lets Encrypt Production Server
    # server: https://acme-v02.api.letsencrypt.org/directory 

    # Staging Server - Use this one first to make sure it's all working.
    # Once it's working, you can uncomment the first one to get a real certificate
    server: https://acme-staging-v02.api.letsencrypt.org/directory 
    privateKeySecretRef:
      name: your-secret-tls-name   # You can name this whatever you want
    solvers:
      - http01:
          ingress:
            class: traefik  # This could also be `nginx` if you're using nginx ingress
---
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: your-cert-name  # This can be whatever you want
spec:
  dnsNames:
    - example.com  # Your host name. (Not sure if this is actually needed or not)
  secretName: your-secret-tls-name  # Should be the same as name of privateKeySecretRef used up above
  issuerRef:
    name: letsencrypt-issuer  # This should match the name of the issuer you created up above
    kind: ClusterIssuer   # Should match the type of issuer you created up above
```

For a set of valid values you can use in your cert-manager yamls, refer to
https://cert-manager.io/docs/reference/api-docs/#cert-manager.io%2fv1 and
https://cert-manager.io/docs/reference/api-docs/
