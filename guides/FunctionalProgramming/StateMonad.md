## The State Monad

### Motivation

In OOP languages, functions have implicit access to state, and can
alter it at will.  

In functional languages, the only data functions have access to are its parameters, and they can't
mutate values, only return data. State would need to be passed to the function, and state changes read from
the return result.

<details>
<summary> OOP vs Functional Comparison </summary>

#### OOP Example: (In C#)
```
public class Foo
{ 
    bool toggle = false;
    int counter = 0;
    //Some constructor here...
    
    //runToggle can see the objects properties
    public bool runToggle()
    {
        toggle = !toggle
        counter += 1;
        
        return toggle
    }
}
```

#### FP Example (In Purescript):
```
type FooData = 
  { toggle :: Boolean
  , counter :: Int
  }
  
{- runToggle needs FooData passed to it
   runToggle can't alter foo, only return a modified copy of it
-}
runToggle :: FooData -> Tuple FooData Boolean
runToggle foo = do
  let toggle' = not foo.toggle
  
  Tuple
    (foo { toggle = toggle'
         , counter = foo.counter+1 
         }
    )
    (not foo.toggle)
```
</details>

Using the `State Monad`, we can hide the explicit state argument, and add the ability to 
read/write state within our functions.

### How do you use it?
---
__We have the type__ `State s a`.

- `s` is the type of our state. E.g: `type FooData { toggle :: Boolean, counter :: Int }`
- `a` is the type of our functions return value.
- `State s a` comes with some helper functions to get/set the state such as `get`, `put`, `modify`, etc.  
   Anything with the type `State s a` can use those get/set functions

If we want our function to have access to state, we can change it's return type to use 
the `State s a` type. E.g: 
- Normal Function:   `addNumbers :: Int -> Int -> Int`
- Stateful Function: `addNumbers :: Int -> Int -> State Int Int`

---
__We have the function__ `runState :: State s a -> s -> Tuple a s`
- `runState` extracts our state and return result from the `State s a` type and returns them in a `Tuple`
- `runState` requires an initial state `s` to run stateful functions

---
__It's a Monad__
  
That means we have all the power of `map`,`apply`, and `bind`.  I'm not going to touch on that here though.  
Read through the `Functor`, `Applicative`, and `Monad` guides if you want to know about how those work. 

---
### Examples
---

##### Ex 1. 
Incrementing a counter whenever our `addNumbers` function is called
```
addNumbers :: Int -> Int -> State Int Int
addNumbers x y = do
  modify' ((+) 1)
  pure $ x + y

doSomeAdding :: IO ()
doSomeAdding = do
  let x = addNumbers 5 5 >>= addNumbers 20
      y = (+) 30 <$> x
      z = (*) <$> y <*> (pure 40)
      (total,counter) = runState z 0

  putStrLn $ "The total is: " <> show total
  putStrLn $ "addNumbers was called: " <> show counter <> " times!"
```
---
##### Ex 2. 
A machine that only adds numbers when the "power" is on
```
newtype Machine = Machine { activated :: Bool }

toggleMachine :: State Machine ()
toggleMachine = do
  modify' (\m -> m { activated = not $ activated m })

machineAdd :: Int -> Int -> State Machine Int
machineAdd x y = do
  machine <- get
  if activated machine
    then pure $ x + y
    else pure 0

machine1 :: State Machine Int
machine1 = do
  w <- machineAdd 10 10
  toggleMachine
  x <- machineAdd 10 20
  y <- machineAdd 30 40
  toggleMachine
  z <- machineAdd 9999 4529
  pure $ w + x + y + z

runTheMachine :: IO ()
runTheMachine = do
  let (total, active) = runState machine1 (Machine False)
  putStrLn $ "TotalX is " <> show total <> ". Machine1 power is " <> (show $ activated active)
```