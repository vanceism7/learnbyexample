## Optics

### Motivation 
It's hard to read/write from deep nested data structures. Optics make it easier!

### How
Optics define functions such as:  
- `view` -- Read/Get,  
- `set` -- Write,  
- `over` -- Update using the current value

These functions take a sort of "path" that gives us access to data within a
structure, and allow us to get or modify that data

### Examples:
Simple optics are hardly useful, but we'll show some


1. `_1` -- The left side of a tuple
```
let tup = Tuple "Hello" 10
    result = view _1 tup                   -- result == "Hello"
    result = set _1 "HELLO" tup            -- result == Tuple "HELLO" 10
    result = over _1 (_ <> " world!") tup  -- result == Tuple "Hello World!" 10
```

2. `_Right` -- The right value of an `Either`
```
let e = Right "Yo"
    val1 = preview _Right e                   - val1 == Just "Yo"
    val2 = set _Right "Bye!" e                -- val2 == Right "Bye!"
    val3 = over _Right (_ <> " Gabba Gabba")  -- val3 == Right "Yo Gabba Gabba"

    bad1 = preview _Left e        -- bad1 == Nothing
    bad2 = set _Left 65 e         -- bad2 == Right "Yo"
    bad3 = over _Left (_ + 10) e  -- bad3 == Right "Yo" 
```

3. `traversed` -- Operate on values from an array
```
-- (Stole this example from pursuit ^_^)

let xs = [1,2,3]
    over    traversed negate xs == [-1, -2, -3]
    preview traversed xs == Just 1
    firstOf traversed xs == Just 1  -- same as `preview`
    lastOf  traversed xs == Just 3
```

So you can see that optics give us a new way to work with our data, but the true power comes from composing these optics together.  

4. A triple nested Tuple
```
let tup = Tuple (Tuple (Tuple 100 unit) "Whoa") True

-- We can modify the left most value like this:
let val1 = over (_1 <<< _1 <<< _1) (_ * 10) tup  -- val1 = Tuple (Tuple (Tuple 1000 unit) "Whoa") true

-- Or grab the "Whoa" string like this:
let val2 = view (_1 <<< _2) tup -- val2 == "Whoa"

-- Or set the unit value to a string
let val3 = set (_1 <<< _1 <<< _2) "Cool beans" tup -- val3 = Tuple (Tuple (Tuple 200 "Cool beans!") "Whoa") true
```

5. Deep nested array filtering.  
Lets say we have some set of social networks of people, and we want remove inactive friends from each persons  
profile...

```
--------------------
-- Our data types --
--------------------
newtype Person = Person
  { name :: String
  , friends :: Array Person
  , points :: Int
  }
derive instance newtypePerson :: Newtype Person _

newtype Network = Network (Array Person)
derive instance newtypeNetwork :: Newtype Network _

newtype Global = Global (Array Network)
derive instance newtypeGlobal :: Newtype Global _

--------------------------------
-- Friends Filtering Function -- 
--------------------------------
removeInactive :: Global -> Global
removeInactive global = do
  let _friends = SProxy :: SProxy "friends"
      path = _Newtype <<< traversed <<< _Newtype <<< traversed <<< _Newtype <<< prop _friends
      
  over path (filter inactive) global
  where
  
  -- Lets say an inactive Person is anyone with less than 100 points
  inactive :: Person -> Boolean
  inactive (Person p) = p.points <= 100
  
-----------
-- Notes --
-----------
-- _Newtype is the optic that unwraps a newtype
-- prop is the optic that gives us a specific field within a record
```
I came across a similar situation to #5 during work recently and the non-lens solution is painful.  
  
## But how does it work!?
You really shouldn't concern yourself with how optics work if you're just starting out.  
Learning how to use them is the best first step you can take. But if you must know, [checkout this guide](https://github.com/hablapps/DontFearTheProfunctorOptics).  
Also, if you're looking for some more indepth explanation on how to use them, [look here](https://thomashoneyman.com/articles/practical-profunctor-lenses-optics/)