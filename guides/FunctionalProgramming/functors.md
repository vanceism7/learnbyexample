## Functors

<details>
<summary> Don't know haskell-ish syntax? Here's what you need to know!</summary>

### Primer
---  
#### Variables
Variables take this form:
```
x :: Int
x = 10

message :: String
message = "Hello"
```
The line `x :: Int` declares `x`'s type. 
- `::` means: is type
- `x :: Int` means: x is type Int
---

#### Functions
Functions take this form:
```
func :: Int -> Int -> String
func x y = "Hello"
```

The line `func :: Int -> Int -> String` declares `func`'s type.
- `Int -> Int -> String` means this function takes two Int arguments, and returns a String.  
- Arguments are separated by `->`. Whatever appears after the last `->` is the return type
- `func :: Int -> Int -> String` means: `func` is a function that takes two ints and returns a string

The line `func x y = "Hello"` shows the functions implementation
- Arguments are separated by spaces. Parenthesys aren't used, commas aren't used
- This function takes two ints and does nothing with them. It always returns "Hello", how useful!

#### Polymorphic types
If you see a function with this type:
```
func :: a -> b
```

This is a polymorphic function, it means
- `a` can be any type
- `b` can be any type.  
  
If you see this:
```
func :: f a -> f b
```

The type `f` is any type which holds another type. For example:
- Arrays - An Array works with many types, like `Array Int`, `Array String`, etc

</details>

### Motivation
Functors allow us to separate our core logic from the context of the data.
The idea is that regardless of the context, the core logic we perform is always the same.
We can safely ignore the context.

### How do you use it?
`Functor` is a sort of `interface` providing the function `map`.
```
map :: (a -> b) -> (f a -> f b)
<$> :: (a -> b) -> (f a -> f b) -- infixed version
``` 
`map` takes a function `(a -> b)` and turns it into a new function `(f a -> f b)`.  
Any type `f` can be a `functor` if it can provide a definition for `map`.  

Don't get too hung up if you don't totally understand this. Intuiton comes from studying the examples.

### Examples
1. Our core logic is a function that squares a number. 
   Our context could be anything like  
- `Maybe Int` -- The context that the `Int` might not exist
- `Array Int` -- The context that we have 0, 1, or more `Ints`
- `Parser Int` -- The context of parsing an `Int` from some data source like a `String`.

```
--------------------
-- The core logic --
--------------------
squareNum :: Int -> Int
squareNum x = x * x

-----------------------
-- The Maybe Context --
-----------------------

-- We can't do this:
val = squareNum (Just 12) -- squareNum takes an Int, not Maybe Int

-- But we can do this:
val = map squareNum (Just 12) -- equals `Just 144`
val = squareNum <$> (Just 12) -- equals `Just 144`

val = map squareNum Nothing -- equals Nothing
val = squareNum <$> Nothing -- equals Nothing 

-----------------------
-- The Array context --
-----------------------

--We can't do this:
val = squareNum [1,2,3] -- squareNum takes Int, not Array Int

-- But we can do this:
_ = map squareNum [1,2,3] -- equals [1,4,9]
_ = squareNum <$> [1,2,3] -- equals [1,4,9]

_ = map squareNum [] -- equals []
_ = squareNum <$> [] -- equals []


------------------------
-- The Parser Context --
------------------------
-- Here's a potential definition for Parser.
data Parser a
  = Parsed a
  | Error

parseInt :: String -> Parser Int
parseInt text = do
  case parseNumber text of -- parseNumber isn't a real function (I don't think)
    Nothing -> Error
    Just num -> Parsed num


--We can't do this:
val = squareNum (parseInt "4") -- squareNum takes Int, not Parser Int

-- But we can do this:
val = map squareNum (parseInt "4") -- equals Parsed 16 
val = squareNum <$> (parseInt "4") -- equals Parsed 16

val = map squareNum (parseInt "hello") -- equals Error
val = squareNum <$> (parseInt "hello") -- equals Error

```

So you can see from above that the definition of `squareNum` didn't have to change in any way to accomodate
for the context of it's argument. It worked in all scenarios none the wiser. This is the power `Functor` brings to the table.

### But how does `map` know!??
`map` looks at it's first argument to figure out `a` and `b`, and it's last argument to figure out the context `f`  
  
e.g:
```
map :: (a -> b) -> (f a -> f b)
map squareNum (Just 12)

--The first argument is (Int -> Int). So a = Int, b = Int, giving us:
map :: (Int -> Int) -> (f Int -> f Int)

-- The last argument is Just 12, so it figures out f = Maybe and it becomes
map :: (Int -> Int) -> (Maybe Int -> Maybe Int)
```

That's it!