## Folds

### Motivation
You want to build up a result by iterating over a collection of data - similar
to a `for` loop

<details>
<summary><b>What do you mean a "collection" of data?</b></summary> 

---
Im talking about stuff like `Arrays`, `Lists`, `Sets`, `Dictionaries`/`Maps`, etc.  
Although we mainly are talking about "collections" of data, any data structure
that implements the `Foldable` interface/typeclass can be folded over.
So you can also fold over types like `Maybe`!
</details>

### How do you use it?
We have the functions  
- `foldl :: forall a b f. Foldable f => (b -> a -> b) -> b -> f a -> b`
- `foldr :: forall a b f. Foldable f => (a -> b -> b) -> b -> f a -> b`  
(There are others but we'll focus on these)  

This definition looks confusing! Renaming the types might help  
```purescript
foldl :: forall a result collection. Foldable collection =>
  (result -> a -> result) -> result -> collection a -> result
```

So using the above definition, `foldl` takes:
- a combining function: `(result -> a -> result)` - which uses an `a` from the collection to alter the result
- an initial `result` value to combine the elements in the collection with
- a collection to iterate over
- and returns a `result`
- Note: `a` and `result` can be the same type

  
Don't worry if that sounds confusing, the examples will make it more clear.  
If you want to see a more explicit break down, checkout `The break down` below

<details>
<summary><b>The break down</b></summary>

#### Lets break this down
<pre>foldl :: <b>forall a result collection.</b> Foldable collection => (result -> a -> result) -> result -> collection a -> result</pre>
- This function is polymorphic for the three types `a`, `result` and `collection`. They an be any type. (Except collection)

<pre>foldl :: forall a result collection. <b>Foldable collection =></b> (result -> a -> result) -> result -> collection a -> result</pre>
- This means `collection` can't be just any type. It must be a type that implements the `Foldable` interface/typeclass.

<pre>foldl :: forall a result collection. Foldable collection => <b>(result -> a -> result)</b> -> result -> collection a -> result</pre>
- The first argument to `foldl` is a function. This function takes a `result` and an `a` from your collection, and returns a new `result`.
- This function analyzes your `a` value to determine what to do with it.
- It could add it to the result, or perhaps ignore it completely (Its totally up to you!)
- Note: `a` and `result` can be the same type

<pre>foldl :: forall a result collection. Foldable collection => (result -> a -> result) -> <b>result</b> -> collection a -> result</pre>
- The next argument to foldl is an initial result value (otherwise, you wouldn't have anything to combine your `a` value with)

<pre>foldl :: forall a result collection. Foldable collection => (result -> a -> result) -> result -> <b>collection a</b> -> result</pre>
- After the initial value, `foldl` expects the actual collection to iterate over.

<pre>foldl :: forall a result collection. Foldable collection => (result -> a -> result) -> result -> collection a -> <b>result</b></pre>
- And finally, it returns you a result
</details>

### Examples
**1. Finding the sum of all elements in an array**

```purescript
findSum :: Array Int -> Int
findSum xs = foldl (+) 0 xs

-- Alternative
findSum2 :: Array Int -> Int
findSum2 xs =
  foldl (\result num -> result + num) 0 xs
  
-- Alternative
adder :: Int -> Int -> Int
adder result num = result + num

findSum3 :: Array Int -> Int
findSum3 xs =
  foldl adder 0 xs
```

Notice that since the combining function is `Int -> Int -> Int`, this causes 
`result` and `a`'s types to both be `Int` here.  
If we substitude `Int` anywhere `result` and `a` appear in `foldl`'s type 
signature, along with `Array` for `collection`, foldl's type signature becomes:  

```purescript
-- The general type signature
foldl :: (result -> a   -> result)  -> result -> Array a   -> result

-- After applying substitutions
foldl :: (Int    -> Int -> Int)     -> Int    -> Array Int -> Int
```

---

**2. Finding the difference of all elements in an array**

```purescript
findDiff :: Array Int -> Int
findDiff xs = foldl (-) 0 xs
```

---

**3. Filter out all numbers less than 10 from an array**  

```purescript
filterSmall :: Array Int -> Array Int
filterSmall xs = 
  foldl filter [] xs
  where
    filter :: Array Int -> Int -> Array Int
    filter result num =
      if num < 10 then result else num : result
```

---

**4. Find all the common elements between a couple arrays**  
E.g: If you have 3 arrays: `[1,2,3], [2,3,4], [3,4,5]`,   
the element they all share in common is 3, so our new array would be `[3]`

```purescript
intersectAll :: Array (Array Int) -> Array Int
intersectAll xs = do
  -- We need to create an array with all possible choices as our initial value
  -- If we started with [], intersect would always result in [] since
  -- an empty array has no common elements with any other array
  let allNumbers = foldl Array.union [] xs
  
  foldl intersectInts allNumbers xs
  where
    intersectInts :: Array Int -> Array Int -> Array Int
    intersectInts result a = Array.intersect result a
```

<details>
<summary><b>What is the type of foldl here?</b></summary>

---
In this case, we look again at the combining function to figure out the 
types of `a` and `result`

```purescript
foldl ::    ... (result    -> a         -> result)    -> result ...
intersectInts :: Array Int -> Array Int -> Array Int
```

We can see that `result` will be `Array Int` and `a` will be `Array Int`,  
so both our `result` and `a` types are the same again: `Array Int`  
  
If `a` is `Array Int`, then what is `collection a`? Well since we're dealing with  
an `Array (Array Int)`, we can see here that `collection` = `Array`.  
Substituting this all back into foldl, we get the following:

```purescript
-- The general type signature
foldl :: (result    -> a         -> result)    -> result    -> Array a           -> result

-- After applying substitutions
foldl :: (Array Int -> Array Int -> Array Int) -> Array Int -> Array (Array Int) -> Array Int
```

</details>

Example 4 is one scenario I came across during work recently. I had an array of 
sets of user permissions, corresponding to the users granted permissions per 
organization. In the UI, if multiple organizations were selected, we needed to 
show only the roles they had in common from all the organizations. The above
was how I solved the problem using folds.












